﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        


        
private void button1_Click(object sender, EventArgs e)
        {
            int count = Convert.ToInt32(textBox1.Text);
            int min = Convert.ToInt32(textBox2.Text);
            int max = Convert.ToInt32(textBox3.Text);
            
            Elem[] array = new Elem[count];
            Random random = new Random();
            for (int i = 0; i < 5; i++)
            {
                
                array[i] = new Elem(random.Next(min,max));
            }

            //Создание DataGridView
            if (max - min + 1 > count)
            {
                for (int i = min; i < max + 5; i++)
                {
                    dataGridView1.Columns.Add("", "");
                }
            }
            else
            {
                for (int i = 0; i < count+5; i++)
                {
                    dataGridView1.Columns.Add("", "");
                }
            }

            for (int i = 0; i < 6; i++)
            {
                dataGridView1.Rows.Add();
            }
            for (int i = 0; i < array.Length; i++)
            {
                dataGridView1.Rows[0].Cells[i].Value = array[i].key;
            }
            int a = min;
            for (int i = 0; i < count; i++)
            {
                dataGridView1.Rows[3].Cells[i].Value = a;
                a++;
            }
            dataGridView1.Refresh();
            //Сортировка
            CountSort sort = new CountSort();
            sort.CountingSort(array, max, min, dataGridView1);
        }
    }

}
