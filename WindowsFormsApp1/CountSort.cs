﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;


namespace WindowsFormsApp1
{

    class CountSort
    {
        
        
        public Elem[] CountingSort(Elem[] array, int max,int min, DataGridView dgv)
        {
            Elem[] countArray = new Elem[max-min+1];
            for(int i = 0; i < countArray.Length; i++)
            {
                
                countArray[i] = new Elem(0);
                SetDgv(countArray[i].key, dgv, 2, i);
            }
            
            for (var i = 0; i < array.Length; i++)
            {
                countArray[array[i].key].key++;
                SetColor(0, i, System.Drawing.Color.Red, dgv);
                Thread.Sleep(200);
                dgv.Refresh();
                SetDgv(countArray[array[i].key].key, dgv, 2, array[i].key - min);
                SetColor(2, array[i].key-min, System.Drawing.Color.Red, dgv);
                Thread.Sleep(200);
                dgv.Refresh();
                SetColor(0, i, System.Drawing.Color.White, dgv);
                SetColor(2, array[i].key-min, System.Drawing.Color.White, dgv);
                Thread.Sleep(200);
                dgv.Refresh();
            }

            var index = 0;
            for (var i = min; i < max+1; i++)
            {
                
                if (countArray[i-min].key != 0)
                {
                    Thread.Sleep(200);
                    SetColor(3, i-min-1, System.Drawing.Color.Red, dgv);
                    dgv.Refresh();
                    int c = countArray[i - min].key-1;
                    for (var j = 0; j < countArray[i-min].key; j++)
                    {
                        SetColor(5, index, System.Drawing.Color.Green, dgv);
                        array[index].key = i-min;
                        SetDgv(i - min, dgv, 5, index);
                        SetDgv(c--, dgv, 2, i - min -1 );
                        dgv.Refresh();
                        Thread.Sleep(200);
                        dgv.Refresh();
                        index++;
                        SetColor(5, index, System.Drawing.Color.White, dgv);
                        Thread.Sleep(200);
                        dgv.Refresh();

                    }
                    SetColor(3, i-min-1, System.Drawing.Color.White, dgv);
                    dgv.Refresh();
                }
                
            }

            return array;
        }
        public static void SetDgv(int a, DataGridView dgv, int r, int c)
        {
            dgv.Rows[r].Cells[c].Value = Convert.ToString(a);
        }


        public static void SetColor(int r, int c, System.Drawing.Color color, DataGridView dgv)
        {
            dgv.Rows[r].Cells[c].Style.BackColor = color;
        }

        public static void ColorArrP(int firstIndex, int lastIndex, int row, System.Drawing.Color color, DataGridView dgv)
        {
            for (int c = firstIndex; c <= lastIndex; c++)
            {
                SetColor(row, c, color, dgv);
            }

        }


    }
}
